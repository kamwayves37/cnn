package convolution;

import filter.Filter;

public class Conv2D extends Conv{
    private Filter filter;
    private Number[][] inputs;

    public Conv2D(Number[][] inputs, Filter filter) {
        this.inputs = inputs;
        this.filter = filter;
        super.initoutput(inputs.length, inputs[0].length, filter.getRow(),
                filter.getCol(), filter.getPadding(),filter.getStride());
    }

    @Override
    public void applyZeroPadding() {
        inputs = super.zeroPadding(inputs, filter.getPadding());
    }

    @Override
    public void activationMap() {
        feature =  super.activationMap(inputs, filter);
    }
}
