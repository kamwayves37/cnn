package convolution;

import activation.ActivationMode;
import connection.ConnectionFactory;
import matrix.Matrix;
import filter.Filter;
import proprieties.Proprieties;

public class Conv3D implements ConnectionFactory {
    private ConvProperties properties;
    private ActivationMode activationMode;
    private Matrix[] output;
    private Filter[][] filters;
    private boolean isInitialize;

    public Conv3D(ConvProperties properties){this.properties = properties;}

    public Conv3D(){
        properties = new ConvProperties();
    }

    public void poolingLayers(int f, int s){
        for(int x = 0; x < filters.length; x++){
            output[x] = output[x].maxPooling(f, s);
        }
    }

    @Override
    public Proprieties getProperties() {
        return properties;
    }

    @Override
    public void setProperties(Proprieties properties) {
        this.properties = (ConvProperties) properties;
    }

    @Override
    public void initProperties() {
        if(!isInitialize){
            filters = new Filter[properties.getNumberOfFilters()][properties.getInputShape().getThird()];
            for(int i = 0; i < filters.length; i++){
                for(int j = 0; j < filters[0].length; j++){
                    this.filters[i][j] = new Filter(properties.getFilterSize().getKey(), properties.getFilterSize().getValue(),
                            properties.getFilterPadding(), properties.getFilterStride());

                    this.filters[i][j].randomize();

                    //this.filters[i][j].setPadding(properties.getFilterPadding());
                }
            }
            this.output = new Matrix[filters.length];

            activationMode = Proprieties.getActivationMode(properties.getActivationType());

            isInitialize = true;
        }
    }

    @Override
    public Matrix[] outPutLayout() {
        return output;
    }

    @Override
    public int numberOfParams() {
        return ((properties.getFilterSize().getKey()*properties.getFilterSize().getKey())
                *properties.getInputShape().getThird()*properties.getNumberOfFilters()) + properties.getNumberOfFilters();
    }

    @Override
    public void activation(Matrix[] inputs) {
        if(!isInitialize)
            initProperties();

        Matrix[] result = new Matrix[properties.getInputShape().getThird()];
        for(int x = 0; x < filters.length; x++){
            for(int i = 0; i < result.length; i++){
                result[i] =  inputs[i].activeFeature(filters[x][i]);
            }

            if(result.length > 1)
                this.output[x] = Matrix.add(result).activate(activationMode);
            else
                this.output[x] = result[0].activate(activationMode);
        }
    }

    @Override
    public String toString() {
        if(output != null){
            return "(" +
                    output[0].getRow() +
                    ", " + output[0].getCol()+
                    ", " + output.length +
                    ')';
        }
        return "(None, None, None)";
    }
}
