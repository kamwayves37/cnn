package convolution;

import activation.ActivationType;
import javafx.util.Pair;
import outils.Triplet;
import pooling.IMaxPoolingProprieties;
import proprieties.Proprieties;

public  class ConvProperties extends Proprieties  implements IConvProprieties {

    private int numberOfFilters;
    private Pair<Integer, Integer> filterSize;
    private int filterPadding;
    private int filterStride;
    private ActivationType activationType;
    private Triplet<Integer, Integer, Integer> inputShape;
    private boolean same;

    public ConvProperties(){
        filterSize = new Pair<>(0,0);
        inputShape = Triplet.create(0,0,0);
        numberOfFilters = 0;
        filterPadding = 0;
        filterStride = 1;
        activationType = ActivationType.Linear;
        same = false;
    }

    @Override
    public ConvProperties setNumberOfFilters(int filterLength) {
        this.numberOfFilters = filterLength;
        return this;
    }

    @Override
    public ConvProperties setFilterSize(Pair<Integer, Integer> filterSize) {
        this.filterSize = filterSize;
        return this;
    }

    @Override
    public ConvProperties setFilterPadding(int filterPadding) {
        this.filterPadding = filterPadding;
        return this;
    }

    @Override
    public ConvProperties setFilterStride(int filterStride) {
        if(filterStride <= 0)
            this.filterStride  = 1;
        else
            this.filterStride = filterStride;

        return this;
    }

    @Override
    public ConvProperties setActivationType(ActivationType activationType) {
        this.activationType = activationType;
        return this;
    }

    @Override
    public ConvProperties activationShape(Triplet<Integer, Integer, Integer> inputShape) {
        this.inputShape = inputShape;
        return this;
    }

    @Override
    public ConvProperties setSame(boolean same) {
        this.same = same;
        return this;
    }

    public int getNumberOfFilters() {
        return numberOfFilters;
    }

    public Pair<Integer, Integer> getFilterSize() {
        return filterSize;
    }

    public int getFilterPadding() {
        if(!same)
            return filterPadding;
        else
            return (inputShape.getFirst()*(filterStride-1) + filterSize.getKey() - filterStride) / 2;
    }

    public int getFilterStride() {
        return filterStride;
    }

    public ActivationType getActivationType() {
        return activationType;
    }

    public Triplet<Integer, Integer, Integer> getInputShape() {
        return inputShape;
    }

    public boolean isSame() {
        return same;
    }
}
