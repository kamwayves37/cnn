package convolution;

public interface IConv {

    void applyZeroPadding();
    void activationMap();
    void initoutput(int inputRow, int inputCol,int filterRow, int filterCol, int padding, int stride);
}
