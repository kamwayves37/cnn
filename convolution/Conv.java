package convolution;

import filter.Filter;

public abstract class Conv implements IConv{

    protected Number[][] feature;

    protected Number[][] zeroPadding(Number[][] inputs, int padding){
        Number[][] zeroPadding = new Number[inputs.length + (padding*2)][inputs[0].length + (padding*2)];

        for(int j = 0; j < zeroPadding.length; j++){
            for(int k = 0; k < zeroPadding[0].length; k++) {
                if((j < padding || j >= inputs.length+padding) || (k < padding || k >= inputs.length+padding))
                    zeroPadding[j][k] = 0;
                else
                    zeroPadding[j][k] = inputs[j-padding][k-padding];
            }
        }

        return zeroPadding;
    }

    protected Number[][] activationMap(Number[][] inputs, Filter filter){
        Number[][] inputsResult = zeroPadding(inputs, filter.getPadding());
        int row = ((inputs.length - filter.getRow() + filter.getPadding()*2)/filter.getStride()) + 1;
        int col = ((inputs[0].length - filter.getCol() + filter.getPadding()*2)/filter.getStride()) + 1;
        Number[][] output = new Number[row][col];

        for(int x = 0; x < output.length; x++){
            for(int y = 0; y < output[0].length; y++){
                int sum = 0;
                for(int j = 0; j < filter.getRow(); j++){
                    for(int k = 0; k < filter.getCol(); k++){
                        //sum += inputsResult[j + (x*filter.getStride())][k+ (y*filter.getStride())].intValue() * filter.getKernel(j,k).intValue();
                    }
                }
                output[x][y] = sum;
            }
        }

        return output;
    }

    @Override
    public void initoutput(int inputRow, int inputCol, int filterRow, int filterCol, int padding, int stride) {
        int row = ((inputRow - filterRow + padding*2)/stride) + 1;
        int col = ((inputCol - filterCol + padding*2)/stride) + 1;
        feature = new Number[row][col];
    }
}
