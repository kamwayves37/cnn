package convolution;

import activation.ActivationType;
import javafx.util.Pair;
import outils.Triplet;
import proprieties.IProprieties;

public interface IConvProprieties extends IProprieties {

    IConvProprieties setNumberOfFilters(int filterLength);

    IConvProprieties setFilterSize(Pair<Integer, Integer> filterSize);

    IConvProprieties setFilterPadding(int filterPadding);

    IConvProprieties setFilterStride(int filterStride);

    IConvProprieties activationShape(Triplet<Integer, Integer, Integer> inputShape);

    IConvProprieties setSame(boolean same);
}
