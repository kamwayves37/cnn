package outils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TripletTest {

    Triplet<Double, Integer, String> triplet;

    @BeforeEach
    void setUp() {
        triplet = Triplet.create(2.4,4,"Bonjour");
    }

    @Test
    void getFirst() {
        assertEquals(2.4, triplet.getFirst());
    }

    @Test
    void getSecond() {
        assertEquals(4, triplet.getSecond());
    }

    @Test
    void getThird() {
        assertEquals("Bonjour", triplet.getThird());
    }

    @Test
    void compareTo() {
        Triplet<Double, Integer, String> cmpTriple = Triplet.create(2.4,4,"Bonjour");

        assertEquals(0, triplet.compareTo(cmpTriple));
    }
}