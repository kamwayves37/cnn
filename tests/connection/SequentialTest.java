package connection;

import activation.ActivationType;
import convolution.Conv3D;
import dense.Dense;
import flatten.Flatten;
import javafx.util.Pair;
import matrix.Matrix;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import outils.Triplet;
import picture.treatment.ImageRGB;
import pooling.MaxPooling2D;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

class SequentialTest {

    @BeforeEach
    void setUp() {

        if(Sequential.size() == -1){
            Sequential.init();

            Sequential.add(new Conv3D())
                    .setNumberOfFilters(32)
                    .setFilterSize(new Pair<>(3,3))
                    .setActivationType(ActivationType.ReLU)
                    .activationShape(Triplet.create(28,28,1))
                    .setFilterStride(1)
                    .setSame(true);

            Sequential.add(new MaxPooling2D(new Pair<>(2,2)));

            Sequential.add(new Conv3D())
                    .setNumberOfFilters(64)
                    .setFilterSize(new Pair<>(3,3))
                    .setActivationType(ActivationType.ReLU)
                    .setSame(true);

            Sequential.add(new MaxPooling2D(new Pair<>(2,2)));

            Sequential.add(new Flatten());
            Sequential.add(new Dense(128, ActivationType.ReLU));
            Sequential.add(new Dense(10, ActivationType.Softmax));
        }

    }

    @Test
    void summary() throws IOException {
        //TEST
        ClassLoader classLoader = getClass().getClassLoader();
        BufferedImage bufferedImage  = ImageIO.read(Objects.requireNonNull(classLoader.getResource("X_train_5.jpeg")));
        ImageRGB imageRGB = new ImageRGB(bufferedImage);


        Matrix inputs = imageRGB.toBlackAndWhite(50);

        //inputs.printArray();

        Sequential.setInputData(new Matrix[]{inputs});
        Sequential.summary();
    }

    @Test
    void print() {
        Sequential.print();
        //Sequential.getCouche(Sequential.size()-2).outPutLayout()[0].printArray();
        //Sequential.getCouche(Sequential.size()-1).outPutLayout()[0].printArray();

    }
}