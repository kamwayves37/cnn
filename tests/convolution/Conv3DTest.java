package convolution;

import activation.ActivationType;
import matrix.Matrix;
import javafx.util.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import outils.Triplet;
import picture.treatment.ImageRGB;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

class Conv3DTest {
    private ImageRGB imageRGB;
    private Conv3D conv3D;

    @BeforeEach
    void setUp() {
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            BufferedImage bufferedImage  = ImageIO.read(Objects.requireNonNull(classLoader.getResource("X_train_5.jpeg")));
            imageRGB = new ImageRGB(bufferedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Matrix[] inputs = new Matrix[1];
        inputs[0] = imageRGB.toBlackAndWhite(50);

        inputs[0].printArray();

        conv3D = new Conv3D();
        conv3D.getProperties()
                .setNumberOfFilters(32)
                .setFilterSize(new Pair<>(3,3))
                .setActivationType(ActivationType.Linear)
                .activationShape(Triplet.create(28,28,1))
                .setSame(true);

        conv3D.activation(inputs);

        System.out.println(inputs[0].getRow() + "x" + inputs[0].getCol() + "x" + inputs.length);
    }

    @Test
    void activationMap() {

        System.out.println(conv3D.outPutLayout()[0].getRow() + "x" + conv3D.outPutLayout()[0].getCol() + "x"
                + conv3D.outPutLayout().length + " Param: " + conv3D.numberOfParams());

        conv3D.poolingLayers(2,2);

        System.out.println(conv3D.outPutLayout()[0].getRow() + "x" + conv3D.outPutLayout()[0].getCol() + "x"
                + conv3D.outPutLayout().length + " Param: 0");

        Conv3D conv3D2 = new Conv3D();

        conv3D2.getProperties()
                .setNumberOfFilters(64)
                .setFilterSize(new Pair<>(3,3))
                .setActivationType(ActivationType.ReLU)
                .setSame(true)
                .activationShape(Triplet.create(conv3D.outPutLayout()[0].getRow(),
                        conv3D.outPutLayout()[0].getCol(),
                        conv3D.outPutLayout().length));

        conv3D2.initProperties();
        conv3D2.activation(conv3D.outPutLayout());

        System.out.println(conv3D2.outPutLayout()[0].getRow() + "x" + conv3D2.outPutLayout()[0].getCol() + "x"
                + conv3D2.outPutLayout().length + " Param: " + conv3D2.numberOfParams());

        conv3D2.poolingLayers(2,2);

        System.out.println(conv3D2.outPutLayout()[0].getRow() + "x" + conv3D2.outPutLayout()[0].getCol() + "x"
                + conv3D2.outPutLayout().length + " Param: 0");

        for(int i = 0; i < conv3D2.outPutLayout().length; i++)
            conv3D2.outPutLayout()[i].printArray();
    }
}