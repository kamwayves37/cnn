package convolution;

import activation.ActivationType;
import activation.ReLU;
import filter.Filter;
import javafx.util.Pair;
import matrix.Matrix;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import outils.Triplet;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

class Conv2DTest {
    Conv2D conv1, conv2;

    @BeforeEach
    void setUp() {
        Number[][] inputs = {{50,200,235,201},{15,135,88,100},{0,42,77,165},{6,108,250,144}};
        Filter filter1 = new Filter(0,1,new int[][]{{-1,-1,-1},{-1,8,-1},{-1,-1,-1}});
        conv1 = new Conv2D(inputs,filter1);

        Filter filter2 = new Filter(1,2, new int[][]{{-1,4},{-3,2}});
        conv2 = new Conv2D(inputs,filter2);


    }

    @Test
    void name() {
        Conv3D conv3D = new Conv3D();
        conv3D.getProperties().setNumberOfFilters(1)
                .setFilterSize(new Pair<>(3, 3))
                .activationShape(Triplet.create(4, 4, 1))
                .setActivationType(ActivationType.ELU)
                .setFilterStride(1)
                .setSame(true);

        conv3D.activation(new Matrix[]{new Matrix(new int[][]{{50,200,235,201},{15,135,88,100},{0,42,77,165},{6,108,250,144}})});
    }

    @Test
    void resultSizeTest() throws NoSuchFieldException, IllegalAccessException {
        Class<?> c = Conv.class;
        Field att = c.getDeclaredField("output");
        att.setAccessible(true);
        Number[][] inputs1 = (Number[][]) att.get(conv1);
        Number[][] inputs2 = (Number[][]) att.get(conv2);

        //CONV1 FILTER1
        assertArrayEquals(new Number[]{2,2}, new Number[]{inputs1.length,inputs1[0].length});

        //CONV2 FILTER2
        assertArrayEquals(new Number[]{3,3}, new Number[]{inputs2.length,inputs2[0].length});
    }

    @Test
    void applyZeroPadding() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        Class<?> c = Conv2D.class;
        conv1.applyZeroPadding();
        conv2.applyZeroPadding();

        Field att = c.getDeclaredField("inputs");
        att.setAccessible(true);
        Number[][] inputResult1 = (Number[][]) att.get(conv1);
        Number[][] inputResult2 = (Number[][]) att.get(conv2);

        assertArrayEquals(new Number[][]{{50,200,235,201},{15,135,88,100},{0,42,77,165},{6,108,250,144}},inputResult1);
        assertArrayEquals(new Number[][]{{0,0,0,0,0,0},{0,50,200,235,201,0},{0,15,135,88,100,0},{0,0,42,77,165,0},{0,6,108,250,144,0},{0,0,0,0,0,0}},inputResult2);
    }

    @Test
    void activationMap() throws NoSuchFieldException, IllegalAccessException {
        conv1.activationMap();
        conv2.activationMap();

        Class<?> c = Conv.class;
        Field att = c.getDeclaredField("output");
        att.setAccessible(true);
        Number[][] input1 = (Number[][]) att.get(conv1);
        Number[][] input2 = (Number[][]) att.get(conv2);

        //FILTER ONE
        assertArrayEquals(new Number[][]{{373,-451},{-343,-416}}, input1);

        //FILTER TWO
        assertArrayEquals(new Number[][]{{100,-130,-603},{60,245,-595},{24,892,-144}},input2);
    }
}