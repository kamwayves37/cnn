package picture.treatment;

import matrix.Matrices;
import matrix.Matrix;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.util.Objects;

class ImageRGBTest {

    private ImageRGB imageRGB;

    public static InputStream resizeImage(InputStream uploadedInputStream, String fileName, int width, int height){
        try {

            BufferedImage image = ImageIO.read(uploadedInputStream);
            Image originalImage = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);

            int type = (image.getType() == 0) ? BufferedImage.TYPE_INT_RGB : image.getType();
            BufferedImage resizedImage = new BufferedImage(width, height, type);

            Graphics2D g2d  = resizedImage.createGraphics();
            g2d.drawImage(originalImage, 0, 0, width, height, null);
            g2d.dispose();
            g2d.setComposite(AlphaComposite.Src);
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            ImageIO.write(resizedImage, fileName.split("\\.")[1], new File("image1.jpg"));
            ImageIO.write(resizedImage, fileName.split("\\.")[1], byteArrayOutputStream);
            return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());

        } catch (IOException e) {
            return uploadedInputStream;
        }
    }

    public static BufferedImage rotate(BufferedImage img, int angle,  String fileName) throws IOException {
        int w = img.getWidth();
        int h = img.getHeight();
        BufferedImage newImage = new BufferedImage(w, h, img.getType());
        Graphics2D g2 = newImage.createGraphics();
        g2.rotate(Math.toRadians(angle), w/2d, h/2d);
        g2.drawImage(img,null,0,0);

        ImageIO.write(newImage, fileName.split("\\.")[1], new File("imageRotation1.jpg"));
        return newImage;
    }

    @Test
    void name() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("cat.jpg");

        inputStream = resizeImage(inputStream, "test.jpg", 150, 150);

        rotate(ImageIO.read(inputStream), 180, "test.jpg");
    }

    public static void copyStream(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @BeforeEach
    void setUp() {

        ClassLoader classLoader = getClass().getClassLoader();

        try {
            BufferedImage bufferedImage  = ImageIO.read(Objects.requireNonNull(classLoader.getResource("X_train_5.jpeg")));

            imageRGB = new ImageRGB(bufferedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void pixelsArray() {
        Number[][][] rst2 = imageRGB.getRGB();

        Matrices.printArray(rst2[0]);
        Matrices.printArray(rst2[1]);
        Matrices.printArray(rst2[2]);

    }

    @Test
    void pixelsMatrix() {
        Matrix[] rst2 = imageRGB.getMatrixRGB();

       rst2[0].printArray();
       rst2[1].printArray();
       rst2[2].printArray();

    }

    @Test
    void pixelsBlackAndWhite() {

        imageRGB.toBlackAndWhite(50).printArray();
    }
}