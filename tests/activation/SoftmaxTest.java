package activation;

import matrix.Matrix;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import picture.treatment.ImageRGB;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

class SoftmaxTest {

    double learningRate = .03;
    double reg = 0.1;

    Matrix y_enc = new Matrix(new double[][]{{1.,  0.,  0.},{0.,  1.,  0.},{ 0.,  0.,  1.}, { 0.,  0.,  1.}});

    Matrix X =  new Matrix(new double[][]{{0.1, 0.5}, {1.1, 2.3}, {-1.1, -2.3}, {-1.5, -2.5}});

    Matrix W =  new Matrix(new double[][]{{0.1, 0.2, 0.3}, {0.1, 0.2, 0.3}});

    Matrix bias = Matrix.fromArray(new double[]{0.01, 0.1, 0.1});

    Softmax softmax;

    @BeforeEach
    void setUp() {
        softmax = new Softmax();
    }

    @Test
    void output() {

        for(int i = 0; i < 1; i++){
            Matrix output_ip = X.dot(W).add(bias);

            Matrix smax = output_ip.activationSoftmax();
            smax.printArray();
            Matrix labels = smax.argmax(1);

            Matrix y = y_enc.argmax(1);
            System.out.println("class labels: ");
            y.printArray();

            System.out.println("softmax");
            smax.printArray();

            System.out.println("predicted class labels: ");
            labels.printArray();


            Matrix xent = crossEntropy(smax ,y_enc);

            System.out.println("Cross Entropy");
            xent.printArray();

            System.out.print("Cost: ");
            double J_cost  = cost(smax ,y_enc);
            System.out.println(J_cost );

            System.out.println("........................");

            Matrix re = getGradients(y, labels);
            re.printArray();

        }

    }

    private Matrix crossEntropy(Matrix output, Matrix target){
        Matrix result = output.log().multiply(target);
        return result.sum(1).multiply(-1);
    }

    private double cost(Matrix output, Matrix target){
        return crossEntropy(output, target).mean();
    }

    private Matrix getGradients(Matrix y, Matrix labels){
        Matrix dz = labels.subtract(y);
        return dz;
    }

    @Test
    void testSigmoide() throws IOException {

        //TEST

        Matrix X = new Matrix(new double[][]{{0.1, 0.5}, {1.1, 2.3}, {-1.1, -2.3}, {-1.5, -2.5}, {0.0, 0.0}});
        Matrix Y = new Matrix(new double[][]{{0.,  1.,  0.,  0.},{1.,  0.,  0.,  0.},{ 0.,  0.,  0.,  1.}, { 0.,  0.,  0.,  1.}, { 0.,  0.,  1.,  0.}});

        Matrix labelsY = Y.argmax(1);
        int m = 5;
        int nodes = 100;
        double learningRate = .03;

        Matrix W1 = new Matrix(2, nodes);
        Matrix b1 = new Matrix(m, nodes);
        W1.randomize();
        b1.randomize();

        Matrix W2 = new Matrix(nodes, 4);
        Matrix b2 = new Matrix(m, 4);
        W2.randomize();
        b2.randomize();

        for(int i = 0; i < 10000; i++){
            Matrix Z1 = X.dot(W1).add(b1);
            Matrix A1 = Z1.activate(new ReLU());

            Matrix Z2 = A1.dot(W2).add(b2);
            Matrix A2 = Z2.activationSoftmax();

            Matrix labels = A2.argmax(1);

            double cost =  cost(A2,  Y);

            //L'erreur quadratique moyenne
            Matrix E = Y.subtract(A2);
            E = E.multiply(E);
            double erreur = E.sum() / m;

            Matrix dZ2 = A2.subtract(Y);
            //derivate
            Matrix dsmax = A2.multiply(A2.subtract(1.0, 0));

            Matrix dW2 = dZ2.multiply(dsmax);
            Matrix db2 = dZ2.div(m);


            Matrix dZ1 = dW2.dot(W2.T());
            Matrix dW1 = dZ1.multiply(A1.derivate(new ReLU()));
            Matrix db1 = dZ1.div(m);

            W1 = W1.subtract(X.T().dot(dW1).multiply(learningRate));
            b1 = b1.subtract(db1.multiply(learningRate));

            W2 = W2.subtract(A1.T().dot(dW2).multiply(learningRate));
            b2 = b2.subtract(db2.multiply(learningRate));

            if(i % 400 == 0){
                System.out.println("===================");
                System.out.println("Erreur = " + erreur);
                System.out.println("Cost = " + cost);
                System.out.println("Prediction ");
                labelsY.printArray();
                labels.printArray();
            }
        }

        Matrix test = new Matrix(5,2);
        test.randomize();
        test.multiply(test);

        Matrix Z1 = test.dot(W1).add(b1);
        Matrix A1 = Z1.activate(new Sigmoid());

        Matrix Z2 = A1.dot(W2).add(b2);
        Matrix A2 = Z2.activationSoftmax();

        Matrix labels = A2.argmax(1);

        System.out.println("====== TEST ====== ");
        labels.printArray();
    }
}