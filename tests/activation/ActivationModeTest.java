package activation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ActivationModeTest {

    ActivationMode activationMode;

    @BeforeEach
    void setUp() {
        activationMode = new ReLU();
    }

    @Test
    void getOutput() {
        assertEquals(0, activationMode.output(-3));
        assertEquals(9, activationMode.output(9));
        assertEquals(0, activationMode.output(0));
    }

    @Test
    void getDerivative() {
        assertEquals(0, activationMode.derivative(-4));
        assertEquals(1, activationMode.derivative(9));
        assertEquals(1, activationMode.derivative(0));
    }
}