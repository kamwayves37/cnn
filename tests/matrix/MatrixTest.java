package matrix;

import javafx.util.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import outils.Triplet;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {
    private Matrix matrixA;
    private Matrix matrixB;

    @BeforeEach
    void setUp() {
        int[][] a = new int[2][2];
        a[0][0] = 1;
        a[0][1] = 2;
        a[1][0] = 2;
        a[1][1] = 2;


        matrixA = new Matrix(a);

        int[][] b = new int[2][2];
        b[0][0] = 0;
        b[0][1] = 1;
        b[1][0] = 2;
        b[1][1] = 3;

        matrixB = new Matrix(b);
    }

    @Test
    void add() {
        Matrix result = matrixA.add(matrixB);
        assertArrayEquals(new double[][]{{1,3},{5,7}},result.matrix);
    }

    @Test
    void subtract() {
        Matrix result = matrixA.subtract(matrixB);
        assertArrayEquals(new double[][]{{1,1},{1,1}},result.matrix);
    }

    @Test
    void multiply() {
        Matrix result = matrixA.multiply(matrixB);
        assertArrayEquals(new double[][]{{0,2},{6,12}},result.matrix);
    }

    @Test
    void dot() {
        Matrix result = matrixA.dot(matrixB);
        //assertArrayEquals(new double[][]{{4,7},{8,15}},result.matrix);

        result.printArray();
    }

    @Test
    void zeroPadding() {
        int[][] inputs = {{50,200,235,201},{15,135,88,100},{0,42,77,165},{6,108,250,144}};
        Matrix m = new Matrix(inputs).zeroPadding(1);

        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                Matrix r = m.resize(new Pair(i,j), Triplet.create(2,2,2));
                r.printArray();
                System.out.println();
            }
        }
    }

    @Test
    void name() {
        int[][] inputs = {{50,200,235,201},{15,135,88,100},{0,42,77,165},{6,108,250,144}};
        Matrix m = new Matrix(inputs);
        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 2; j++){
                Matrix r = m.resize(new Pair(i,j), Triplet.create(3,3,1));
                r.printArray();
                System.out.println();
            }
        }
    }

    @Test
    void matrix() {
        Matrix A = new Matrix(new double[][]{{1,2,3}, {4,5,6}, {7,8,9}});
        Matrix B = new Matrix(new double[][]{{7,8,9}, {4,5,6}, {1,2,3} });

        A.dot(B).printArray();
    }
}