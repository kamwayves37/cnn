package pooling;

import javafx.util.Pair;
import proprieties.Proprieties;

public final class MaxPoolingProprieties extends Proprieties implements IMaxPoolingProprieties  {
    Pair<Integer, Integer> poolSize;

    @Override
    public MaxPoolingProprieties setPoolSize(Pair<Integer, Integer> poolSize) {
        this.poolSize = poolSize;
        return this;
    }

    public Pair<Integer, Integer> getPoolSize() {
        return poolSize;
    }
}
