package pooling;

import connection.ConnectionFactory;
import javafx.util.Pair;
import matrix.Matrix;
import proprieties.Proprieties;

public class MaxPooling2D implements ConnectionFactory {

        private Matrix[] output;
    private MaxPoolingProprieties properties;

    public MaxPooling2D(){
        properties = new MaxPoolingProprieties();
    }

    public MaxPooling2D(Pair<Integer, Integer> poolSize) {
        this();
        properties.poolSize = poolSize;
    }

    @Override
    public Proprieties getProperties() {
        return properties;
    }

    @Override
    public void setProperties(Proprieties properties) {
        this.properties = (MaxPoolingProprieties) properties;
    }

    @Override
    public void initProperties() {

    }

    @Override
    public Matrix[] outPutLayout() {
        return output;
    }

    @Override
    public int numberOfParams() {
        return 0;
    }



    @Override
    public void activation(Matrix[] inputs) {
        output = new Matrix[inputs.length];
        for(int x = 0; x < inputs.length; x++){
            output[x] = inputs[x].maxPooling(properties.getPoolSize().getKey(), properties.getPoolSize().getValue());
        }
    }

    @Override
    public String toString() {
        if(output != null){
            return "(" +
                    output[0].getRow() +
                    ", " + output[0].getCol()+
                    ", " + output.length +
                    ')';
        }
        return "(None, None, None)";
    }
}
