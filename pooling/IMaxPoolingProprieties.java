package pooling;

import javafx.util.Pair;
import proprieties.Proprieties;

public interface IMaxPoolingProprieties {

    IMaxPoolingProprieties setPoolSize(Pair<Integer, Integer> poolSize);
}
