package matrix;

import activation.ActivationMode;
import filter.Filter;
import javafx.util.Pair;
import outils.Triplet;

public class Matrix {
    protected int row;
    protected int col;
    protected double[][] matrix;

    public Matrix(int row, int col){
        this.row = row;
        this.col = col;
        matrix = new double[row][col];
    }

    public Matrix(){}

    public Matrix(double[]... tabs){
        this.row = tabs.length;
        this.col = tabs[0].length;
        matrix = new double[row][col];

        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                matrix[i][j] = tabs[i][j];
            }
        }
    }

    public Matrix(int[]... tabs){
        this.row = tabs.length;
        this.col = tabs[0].length;
        matrix = new double[row][col];

        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                matrix[i][j] = tabs[i][j];
            }
        }
    }

    public void setMatrix(double[][] tabs){
        matrix = tabs;
        row = tabs.length;
        col = tabs[0].length;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public void randomize() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = (float) Math.random() * 2 - 1;
            }
        }
    }

    public Matrix T() {
        Matrix result = new Matrix(this.col, this.row);
        for (int x = 0; x < this.row; x++) {
            for (int y = 0; y < this.col; y++) {
                result.matrix[y][x] = this.matrix[x][y];
            }
        }
        return result;
    }

    public Matrix  dot(Matrix m){
        if(col != m.row){
            return null;
        }else{
            Matrix result = new Matrix(row,m.col);

            for (int i = 0; i < row; i++){
                for (int j = 0; j < m.col; j++){
                    double sum = 0;
                    for (int k = 0; k < col; k++){
                        sum += (matrix[i][k] * m.matrix[k][j]);
                    }
                    result.matrix[i][j] = sum;
                }
            }
            return result;
        }
    }

    public Matrix add(Matrix m){
        Matrix result = new Matrix(row,col);
        if(row == m.row && col == m.col){
            for(int i=0; i<row; i++){
                for (int j=0; j<col; j++){
                    result.matrix[i][j] = matrix[i][j] + m.matrix[i][j];
                }
            }
        }else if(col == m.row && m.col == 1){
            for(int i=0; i<row; i++){
                for (int j=0; j<col; j++){
                    result.matrix[i][j] = matrix[i][j] + m.matrix[j][0];
                }
            }
        }else if(row == m.row && m.col == 1){
            for(int i=0; i<row; i++){
                for (int j=0; j<col; j++){
                    result.matrix[i][j] = matrix[i][j] + m.matrix[i][0];
                }
            }
        }
        else
            throw new IllegalArgumentException("The matrices are not of the same order");

        return result;
    }

    public static Matrix add(Matrix... m){
        Matrix result = new Matrix(m[0].row,m[0].col);
        for(int i=0; i<result.row; i++){
            for (int j=0; j<result.col; j++){
                double sum = 0;
                for(int x = 0; x < m.length; x++){
                    sum += m[x].matrix[i][j];
                }
                result.matrix[i][j] = sum;
            }
        }
        return result;
    }

    public Matrix subtract(Matrix m){
        Matrix result = new Matrix(row,col);
        if(row == m.row && col == m.col){
            for(int i=0; i<row; i++){
                for (int j=0; j<col; j++){
                    result.matrix[i][j] = matrix[i][j] - m.matrix[i][j];
                }
            }
        }
        else
            throw new IllegalArgumentException("The matrices are not of the same order");

        return result;
    }

    public Matrix multiply(Matrix m){
        Matrix result = new Matrix(row,col);
        if(row == m.row && col == m.col){
            for(int i=0; i<row; i++){
                for (int j=0; j<col; j++){
                    result.matrix[i][j] = matrix[i][j] * m.matrix[i][j];
                }
            }
        }
        else
            throw new IllegalArgumentException("The matrices are not of the same order");

        return result;
    }

    public Matrix activeFeature(Filter filter) {
        Matrix inputsResult = zeroPadding(filter.getPadding());
        int row = ((this.row - filter.getRow() + filter.getPadding()*2)/filter.getStride()) + 1;
        int col = ((this.col - filter.getCol() + filter.getPadding()*2)/filter.getStride()) + 1;
        Matrix output = new Matrix(row, col);

        for(int x = 0; x < output.row; x++){
            for(int y = 0; y < output.col; y++){
                int sum = 0;
                for(int j = 0; j < filter.getRow(); j++){
                    for(int k = 0; k < filter.getCol(); k++){
                        sum += inputsResult.matrix[j + (x*filter.getStride())][k+ (y*filter.getStride())] * filter.matrix[j][k];
                    }
                }
                output.matrix[x][y] = (sum + filter.getBias());
            }
        }

        return output;
    }

    public double max(){
        double max = matrix[0][0];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if(max < matrix[i][j])
                    max = matrix[i][j];
            }
        }
        return max;
    }

    public Matrix maxPooling(int f, int s) {
        int row = ((this.row - f)/s) + 1;
        int col = ((this.col - f)/s) + 1;
        Matrix result = new Matrix(row, col);
        for(int x = 0; x < result.row; x++){
            for(int y = 0; y < result.col; y++){
                double max = matrix[(x*s)][(y*s)];
                for(int j = 0; j < f; j++){
                    for(int k = 0; k < f; k++){
                        if(max < matrix[j + (x*s)][k+ (y*s)])
                            max = matrix[j + (x*s)][k+ (y*s)];
                    }
                }
                result.matrix[x][y] = max;
            }
        }
        return result;
    }

    public double[] flatten() {
        double[] arr = new double[row * col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                arr[j + i * col] = matrix[i][j];
            }
        }
        return arr;
    }

    public Matrix resize(Pair<Integer, Integer> pos, Triplet <Integer, Integer, Integer> filterSize) {
        Matrix result = new Matrix(filterSize.getFirst(),filterSize.getSecond());
        for(int i=0; i<result.row; i++) {
            for (int j = 0; j < result.col; j++) {
                result.matrix[i][j] = matrix[i + (pos.getKey()*filterSize.getThird())][j + (pos.getValue()*filterSize.getThird())];
            }
        }
        return result;
    }

    public static Matrix fromArray(double[] arr){
        Matrix result = new Matrix(arr.length, 1);
        for (int i = 0; i < result.row; i++){
            result.matrix[i][0] = arr[i];
        }
        return result;
    }

    public Matrix zeroPadding(int padding){
        Matrix result = new Matrix(row + (padding*2),col + (padding*2));
        for(int i=0; i<result.row; i++){
            for (int j=0; j<result.col; j++){

                if((i < padding || i >= row+padding) || (j < padding || j >= col+padding))
                    result.matrix[i][j] = 0;
                else
                    result.matrix[i][j] = matrix[i-padding][j-padding];
            }
        }
        return result;
    }

    //OPERATION VECTORIEL
    public Matrix add(double x){
        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = matrix[i][j] + x;
            }
        }
        return result;
    }

    public Matrix multiply(double x){
        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = matrix[i][j] * x;
            }
        }
        return result;
    }

    public Matrix subtract(double x){
        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = matrix[i][j] - x;
            }
        }
        return result;
    }

    public Matrix subtract(double x, int axis){
        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = x - matrix[i][j];
            }
        }
        return result;
    }

    public Matrix div(double x){
        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = matrix[i][j] / x;
            }
        }
        return result;
    }

    public Matrix activate(ActivationMode activationMode){
        if(activationMode == null)
            throw new NullPointerException("activation model in null");

        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = activationMode.output(matrix[i][j]);
            }
        }

        return result;
    }

    public Matrix derivate(ActivationMode activationMode){
        if(activationMode == null)
            throw new NullPointerException("activation model in null");

        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = activationMode.derivative(matrix[i][j]);
            }
        }

        return result;
    }

    public Matrix exp(){
        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = Math.exp( matrix[i][j]);
            }
        }

        return result;
    }

    public double sum(){
        double sum = 0;
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                sum += matrix[i][j];
            }
        }
        return sum;
    }

    public void printArray(){
        System.out.println("[");
        for (int i = 0; i < row; i++) {
            System.out.print(" [ ");
            for (int j = 0; j < col; j++) {
                System.out.print((int)matrix[i][j] + " ");
            }
            System.out.println("]");
        }
        System.out.println("]");
    }

    //TESTsssss
    public Matrix activationSoftmax(){
        Matrix result = new Matrix(row, col);

        for(int i=0; i<row; i++){
            Matrix hnext = new Matrix(matrix[i]);
            double max = hnext.max();
            double sum = hnext.subtract(max).exp().sum();
            for (int j=0; j<col; j++){
                result.matrix[i][j] = (Math.exp((matrix[i][j] - max)) / sum);
            }
        }

        return result;
    }

    //FONCTION ANTROPI CROISE
    public Matrix argmax(int axis){
        Matrix result = null;

        switch (axis){
            case 0:
                result = new Matrix(col, 1);
                for(int i=0; i<col; i++){
                    double max = matrix[0][i];
                    int index = 0;
                    for (int j=0; j<row; j++){
                        if(max < matrix[j][i]){
                            max = matrix[j][i];
                            index = j;
                        }
                    }
                    result.matrix[i][0] = index;
                }
                break;
            case 1:
                result = new Matrix(row, 1);
                for(int i=0; i<row; i++){
                    double max = matrix[i][0];
                    int index = 0;
                    for (int j=0; j<col; j++){
                        if(max < matrix[i][j]){
                            max = matrix[i][j];
                            index = j;
                        }
                    }
                    result.matrix[i][0] = index;
                }
                break;
            default:
                System.out.println("Aucun choix correspondant");
                break;
        }

        return result;
    }

    public double argmax(){
        double max = matrix[0][0];
        int index = 0;
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                if(max < matrix[i][j]){
                    max = matrix[i][j];
                    index = ((i*(col))+j);
                }
            }
        }
        return index;
    }

    public Matrix log(){
        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = Math.log( matrix[i][j]);
            }
        }

        return result;
    }

    public Matrix sum(int axis) {
        Matrix result = null;

        switch (axis) {
            case 0:
                result = new Matrix(col, 1);
                for (int i = 0; i < col; i++) {
                    double sum = 0;
                    for (int j = 0; j < row; j++) {
                        sum += matrix[j][i];
                    }
                    result.matrix[i][0] = sum;
                }
                break;
            case 1:
                result = new Matrix(row, 1);
                for (int i = 0; i < row; i++) {
                    double sum = 0;
                    for (int j = 0; j < col; j++) {
                        sum += matrix[i][j];
                    }
                    result.matrix[i][0] = sum;
                }
                break;
            default:
                System.out.println("Aucun choix correspondant");
                break;
        }

        return result;
    }

    public double mean(){
        double sum = 0;
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                sum += matrix[i][j];
            }
        }
        return sum/(row*col);
    }

    //TESSSS
    public Matrix arange(Matrix m){

        Matrix result = new Matrix(row,col);
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                result.matrix[i][j] = matrix[i][j] - m.matrix[j][0];
            }
        }

        return result;
    }
}
