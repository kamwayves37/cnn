package matrix;

public class Matrices {

    public static Number[][] dot(Number[][] a, Number[][] b){
        Number[][] r = new Number[a.length][b[0].length];
        if(a[0].length == b.length){
            for (int i = 0; i < a.length; i++){
                for (int j = 0; j < b[0].length; j++){
                    int sum = 0;
                    for (int k = 0; k < a[0].length; k++){
                        if(a[i][k] instanceof  Double && b[k][j] instanceof  Double){
                            sum += (a[i][k].doubleValue() * b[k][j].doubleValue());
                        } else if (a[i][k] instanceof Integer && b[k][j] instanceof  Integer) {
                            sum += (a[i][k].intValue() * b[k][j].intValue());
                        } else if (a[i][k] instanceof Float && b[k][j] instanceof  Float) {
                            sum += (a[i][k].floatValue() * b[k][j].floatValue());
                        }else {
                            throw new IllegalArgumentException("Type " + a[i][j].getClass() + " is not supported by this method");
                        }
                    }
                    r[i][j] = sum;
                }
            }
        }else {
            throw new IllegalArgumentException("The matrices are not of the same order");
        }

        return r;
    }

    public static Number[][] add(Number[][] a, Number[][] b){
        Number[][] r = new Number[a.length][a[0].length];
        if(a.length == b.length && a[0].length == b[0].length){
            for(int i=0; i<a.length; i++){
                for (int j=0; j<a[0].length; j++){
                    if(a[i][j] instanceof  Double){
                        r[i][j] = a[i][j].doubleValue() + b[i][j].doubleValue();
                    } else if (a[i][j] instanceof Integer) {
                        r[i][j]= a[i][j].intValue() + b[i][j].intValue();
                    } else if (a[i][j] instanceof Float) {
                        r[i][j]= a[i][j].floatValue() + b[i][j].floatValue();
                    }else {
                        throw new IllegalArgumentException("Type " + a[i][j].getClass() + " is not supported by this method");
                    }
                }
            }
        }else {
            throw new IllegalArgumentException("The matrices are not of the same order");
        }

        return r;
    }

    public static Number[][] subtract(Number[][] a, Number[][] b){
        Number[][] r = new Number[a.length][a[0].length];
        if(a.length == b.length && a[0].length == b[0].length){
            for(int i=0; i<a.length; i++){
                for (int j=0; j<a[0].length; j++){
                    if(a[i][j] instanceof  Double){
                        r[i][j] = a[i][j].doubleValue() - b[i][j].doubleValue();
                    } else if (a[i][j] instanceof Integer) {
                        r[i][j]= a[i][j].intValue() - b[i][j].intValue();
                    } else if (a[i][j] instanceof Float) {
                        r[i][j]= a[i][j].floatValue() - b[i][j].floatValue();
                    }else {
                        throw new IllegalArgumentException("Type " + a[i][j].getClass() + " is not supported by this method");
                    }
                }
            }
        }else {
            throw new IllegalArgumentException("The matrices are not of the same order");
        }

        return r;
    }

    public static Number[][] multiply(Number[][] a, Number[][] b){
        Number[][] r = new Number[a.length][a[0].length];
        if(a.length == b.length && a[0].length == b[0].length){
            for(int i=0; i<a.length; i++){
                for (int j=0; j<a[0].length; j++){
                    if(a[i][j] instanceof  Double){
                        r[i][j] = a[i][j].doubleValue() * b[i][j].doubleValue();
                    } else if (a[i][j] instanceof Integer) {
                        r[i][j]= a[i][j].intValue() * b[i][j].intValue();
                    } else if (a[i][j] instanceof Float) {
                        r[i][j]= a[i][j].floatValue() * b[i][j].floatValue();
                    }else {
                        throw new IllegalArgumentException("Type " + a[i][j].getClass() + " is not supported by this method");
                    }
                }
            }
        }else {
            throw new IllegalArgumentException("The matrices are not of the same order");
        }

        return r;
    }

    public static Number[][] add(Number[][] a, Number b){
        Number[][] r = new Number[a.length][a[0].length];
        for(int i=0; i<a.length; i++){
            for (int j=0; j<a[0].length; j++){
                if(a[i][j] instanceof  Double && b instanceof Double){
                    r[i][j] = a[i][j].doubleValue() + b.doubleValue();
                } else if (a[i][j] instanceof Integer && b instanceof Integer) {
                    r[i][j]= a[i][j].intValue() + b.intValue();
                } else if (a[i][j] instanceof Integer && b instanceof Double) {
                    r[i][j]= a[i][j].floatValue() + b.doubleValue();
                } else if (a[i][j] instanceof Float) {
                    r[i][j]= a[i][j].floatValue() + b.floatValue();
                }else {
                    throw new IllegalArgumentException("Type " + a[i][j].getClass() + " is not supported by this method");
                }
            }
        }
        return r;
    }

    public static void printArray(Number[][] tabs){
        System.out.println("[");
        for (Number[] tab : tabs) {
            System.out.print(" [ ");
            for (int j = 0; j < tabs[0].length; j++) {
                System.out.print(tab[j] + " ");
            }
            System.out.println("]");
        }
        System.out.println("]");
    }
}
