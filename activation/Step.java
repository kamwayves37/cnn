package activation;

public class Step extends ActivationMode {
    @Override
    public double output(double value) {
        if(value > 0)
            return 0;
        else
            return 1;
    }

    @Override
    public double derivative(double value) {
        return 0;
    }
}
