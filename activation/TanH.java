package activation;

public class TanH extends ActivationMode {
    @Override
    public double output(double x) {
        return (float) Math.tanh(x);
    }

    @Override
    public double derivative(double x) {
        return 1 / (Math.pow((float) Math.cosh(x), 2));
    }
}
