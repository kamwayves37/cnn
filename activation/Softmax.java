package activation;

import matrix.Matrix;

public class Softmax extends ActivationMode  {

    private double sum;
    private double max;

    @Override
    public void sum(Matrix input) {
        max = input.max();

        sum = input.subtract(max).exp().sum();
    }

    @Override
    public double output(double value) {
        return (Math.exp((value - max)) / sum);
    }

    @Override
    public double derivative(double value) {
        return 0;
    }

}
