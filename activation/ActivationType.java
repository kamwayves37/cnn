package activation;

public enum ActivationType {
    ELU,
    Linear,
    PReLU,
    ReLU,
    SeLU,
    Sigmoid,
    Softmax,
    Step,
    TanH,
}
