package activation;

public class ReLU extends ActivationMode {

    @Override
    public double output(double value) {
        return Math.max(0, value);
    }

    @Override
    public double derivative(double value) {
        if(value >= 0)
            return 1;
        else
            return 0;
    }
}
