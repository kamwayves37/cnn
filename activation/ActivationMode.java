package activation;

import matrix.Matrix;

public abstract class ActivationMode {

     public abstract double output(double value);

     public  Matrix output(Matrix value){return value;}

     public abstract double derivative(double value);

     public void sum(Matrix value){}
 }
