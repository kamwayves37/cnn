package activation;

public class Linear extends ActivationMode {
    @Override
    public double output(double value) {
        return value;
    }

    @Override
    public double derivative(double value) {
        return value;
    }
}
