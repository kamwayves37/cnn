package activation;

public class Sigmoid extends ActivationMode {
    @Override
    public double output(double x) {
        return (1d / (1+(float)Math.exp(-x)));
    }

    @Override
    public double derivative(double y) {
        return (y * (1-y));
    }
}
