package flatten;

import connection.ConnectionFactory;
import matrix.Matrix;
import proprieties.Proprieties;

public class Flatten implements ConnectionFactory {

    private Matrix output;

    public Flatten(){
        output = new Matrix();
    }

    @Override
    public Proprieties getProperties() {
        return null;
    }

    @Override
    public void setProperties(Proprieties properties) {

    }

    @Override
    public void initProperties() {

    }

    @Override
    public Matrix[] outPutLayout() {
        return new Matrix[]{output};
    }

    @Override
    public int numberOfParams() {
        return 0;
    }

    @Override
    public void activation(Matrix[] inputs) {
        double[][] result  = new double[inputs.length * inputs[0].getRow() * inputs[0].getCol()][1];

        for (int i = 0; i < inputs.length; i++) {
            double[] temp = inputs[i].flatten();
            for (int j = 0; j < temp.length; j++){
                result[i*temp.length + j][0] = temp[j];
            }
        }

        output.setMatrix(result);
    }

    @Override
    public String toString() {
        if(output != null){
            return "      (" + output.getRow() + ')';
        }
        return "(None, None, None)";
    }
}
