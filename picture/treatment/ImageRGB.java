package picture.treatment;

import matrix.Matrix;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageRGB {
    private int width;
    private int height;
    private boolean hasAlpha;
    private int pixelLength;

    BufferedImage image;

    public ImageRGB(BufferedImage image){
        if(image == null)
            throw new AssertionError("L'image est null");

        this.image = image;
        width = image.getWidth();
        height = image.getHeight();
        hasAlpha = image.getAlphaRaster() != null;

    }

    public Number[][][] getRGB(){
        Number[][][] result = new Number[3][width][height];
        int w = image.getWidth();
        int h = image.getHeight();

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                result[0][j][i] = (image.getRGB(j,i) >> 16) & 0xff;
                result[1][j][i] = (image.getRGB(j,i) >> 8) & 0xff;
                result[2][j][i] = (image.getRGB(j,i)) & 0xff;
            }
        }

        return result;
    }

    public Matrix[] getMatrixRGB(){
        int[][][] result = new int[3][width][height];
        Matrix[] inputs = new Matrix[3];

        int w = image.getWidth();
        int h = image.getHeight();

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                result[0][j][i] = ((image.getRGB(j,i) >> 16) & 0xff);
                result[1][j][i] = ((image.getRGB(j,i) >> 8) & 0xff);
                result[2][j][i] = ((image.getRGB(j,i)) & 0xff );
            }
        }

        inputs[0] = new Matrix(result[0]);
        inputs[1] = new Matrix(result[1]);
        inputs[2] = new Matrix(result[2]);

        return inputs;
    }

    public Matrix toBlackAndWhite(int precision){
        int w = image.getWidth();
        int h = image.getHeight();
        int[][] result = new int[width][height];

        precision = (0 <= precision && precision <= 100) ? precision : 50;

        int limit = 255 * precision / 100;

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                Color color = new Color(image.getRGB(i, j));
                if(limit <= color.getRed() || limit <= color.getGreen() || limit <= color.getBlue())
                    result[j][i] = 1;
                else
                    result[j][i] = 0;
            }
        }

        return new Matrix(result);
    }
}
