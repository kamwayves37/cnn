package connection;

import matrix.Matrix;
import proprieties.Proprieties;

public interface ConnectionFactory {

    Proprieties getProperties();

    void setProperties(Proprieties properties);

    void initProperties();

    Matrix[] outPutLayout();

    int numberOfParams();

    void activation(Matrix[] inputs);
 }
