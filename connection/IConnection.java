package connection;

public interface IConnection {

    public boolean hasNext();
}
