package connection;

import convolution.Conv3D;
import matrix.Matrix;
import outils.Triplet;
import proprieties.Proprieties;

import java.util.ArrayList;

public abstract class Sequential {

    private static ArrayList<ConnectionFactory> connectionFactories;
    private static Matrix[] inputs;

    public static void init() {
        if(connectionFactories == null)
            connectionFactories = new ArrayList<>();
    }

    public static boolean isEmpty(){return connectionFactories == null;};

    public static void add(int index, ConnectionFactory element) {
        connectionFactories.add(index, element);
    }

    public static Proprieties add(ConnectionFactory connectionFactory) {
        connectionFactories.add(connectionFactory);
        return connectionFactory.getProperties();
    }

    public static void add(ConnectionFactory connectionFactory, Proprieties properties) {
        init();
        connectionFactory.setProperties(properties);
        connectionFactories.add(connectionFactory);
    }

    public static ConnectionFactory getCouche(int i) {
        return connectionFactories.get(i);
    }

    public static int size() {
        return (connectionFactories == null ? -1 : connectionFactories.size());
    }

    //TEST
    public static void setInputData(Matrix[] data){
        inputs = data;
    }

    public static void summary(){
        connectionFactories.get(0).activation(inputs);

        for (int i = 1; i < size(); i++) {
            if(connectionFactories.get(i) instanceof Conv3D){
                connectionFactories.get(i).getProperties().activationShape(Triplet.create(
                        connectionFactories.get(i-1).outPutLayout()[0].getRow(),
                        connectionFactories.get(i-1).outPutLayout()[0].getCol(),
                        connectionFactories.get(i-1).outPutLayout().length
                ));
            }
            connectionFactories.get(i).activation(connectionFactories.get(i-1).outPutLayout());
        }
    }

    public static void print(){
        int params = 0;
        System.out.println("============================================================================================");
        System.out.println("Layers (type)                         Output Shape                  Param #");
        System.out.println("============================================================================================");
        for (ConnectionFactory factory : connectionFactories){
            System.out.println(factory.getClass().getName() + "                    " +
                    factory.toString() + "                    "+
                    factory.numberOfParams());
            System.out.println("----------------------------------------------------------------------------------");
            params += factory.numberOfParams();
        }
        System.out.println("TOTAL PARAM: " + params);
    }
}
