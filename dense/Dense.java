package dense;

import activation.ActivationMode;
import activation.ActivationType;
import activation.Softmax;
import connection.ConnectionFactory;
import matrix.Matrix;
import proprieties.Proprieties;

public class Dense implements ConnectionFactory {

    private Matrix weight;
    private Matrix output;
    private Matrix bias;
    private int oNodes;
    private int iNodes;
    private ActivationMode activationMode;
    private ActivationType activationType;
    private boolean isInitialize;

    public Dense(int oNodes, ActivationType activationType) {
        this.oNodes = oNodes;
        this.activationType = activationType;
    }

    @Override
    public Proprieties getProperties() {
        return null;
    }

    @Override
    public void setProperties(Proprieties properties) {

    }

    @Override
    public void initProperties() {
        weight = new Matrix(oNodes, iNodes);
        weight.randomize();
        bias = new Matrix(oNodes, 1);
        bias.randomize();
        activationMode = Proprieties.getActivationMode(activationType);
        isInitialize = true;
    }


    @Override
    public int numberOfParams() {
        return (weight.getRow()*weight.getCol())+bias.getRow();
    }

    @Override
    public Matrix[] outPutLayout() {
        return new Matrix[]{output};
    }

    private Matrix feedForward(Matrix input){
        return weight.dot(input);
    }

    @Override
    public void activation(Matrix[] inputs) {
        if(!isInitialize){
            iNodes = inputs[0].getRow();
            initProperties();
        }

        output = feedForward(inputs[0]);

        if(activationMode instanceof Softmax)
            activationMode.sum(output);

        output.add(bias);

        output = output.activate(activationMode);
    }

    @Override
    public String toString() {
        if(output != null){
            return "      (" + oNodes + ')';
        }
        return "(None, None, None)";
    }
}
