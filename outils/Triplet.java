package outils;

import java.io.Serializable;
import java.util.Objects;

public class Triplet<F, S, T> implements Comparable<Triplet<F, S, T>>, Serializable {
    private final F first;
    private final S second;
    private final T third;

    public Triplet(F first, S second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public F getFirst() {
        return first;
    }

    public S getSecond() {
        return second;
    }

    public T getThird() {
        return third;
    }

    @Override
    public int compareTo(Triplet<F, S, T> other) {
        @SuppressWarnings("unchecked")
        final Comparable<Object> comparableFirst = (Comparable<Object>) getFirst();
        int cmpFirst = comparableFirst.compareTo(other.getFirst());

        if(cmpFirst != 0)
            return cmpFirst;

        @SuppressWarnings("unchecked")
        final Comparable<Object> comparableSecond = (Comparable<Object>) getSecond();
        int cmpSecond= comparableSecond.compareTo(other.getSecond());

        if(cmpSecond != 0)
            return cmpFirst;

        @SuppressWarnings("unchecked")
        final Comparable<Object> comparableThird = (Comparable<Object>) getThird();

        return comparableThird.compareTo(other.getThird());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triplet<?, ?, ?> triplet = (Triplet<?, ?, ?>) o;
        return Objects.equals(first, triplet.first) &&
                Objects.equals(second, triplet.second) &&
                Objects.equals(third, triplet.third);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second, third);
    }

    public static <F, S, T> Triplet<F, S, T> create(F f, S s, T t){
        return new Triplet<>(f,s,t);
    }
}
