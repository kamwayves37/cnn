package proprieties;

import activation.ActivationMode;
import activation.ActivationType;
import convolution.IConvProprieties;
import javafx.util.Pair;
import outils.Triplet;
import pooling.IMaxPoolingProprieties;

public abstract class Proprieties implements IProprieties, IConvProprieties, IMaxPoolingProprieties {

    public static ActivationMode getActivationMode(ActivationType type){
        ActivationMode activationMode = null;

        try {
            Class<?> cp = Class.forName(ActivationMode.class.getPackageName()+"."+ type.toString());
            activationMode =  (ActivationMode) cp.getDeclaredConstructor().newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return activationMode;
    }

    @Override
    public IConvProprieties setNumberOfFilters(int filterLength) {
        return null;
    }

    @Override
    public IConvProprieties setFilterSize(Pair<Integer, Integer> filterSize) {
        return null;
    }

    @Override
    public IConvProprieties setFilterPadding(int filterPadding) {
        return null;
    }

    @Override
    public IConvProprieties setFilterStride(int filterStride) {
        return null;
    }

    @Override
    public IConvProprieties activationShape(Triplet<Integer, Integer, Integer> inputShape) {
        return null;
    }

    @Override
    public IConvProprieties setSame(boolean same) {
        return null;
    }

    @Override
    public Proprieties setActivationType(ActivationType activationType) {
        return null;
    }

    @Override
    public IMaxPoolingProprieties setPoolSize(Pair<Integer, Integer> poolSize) {
        return null;
    }
}