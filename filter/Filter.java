package filter;

import matrix.Matrix;

import java.util.Random;

public class Filter extends Matrix {
    private int padding;
    private int stride;
    private double bias;

    public Filter(int row, int col, int padding, int stride) {
        super(row, col);
        this.padding = padding;
        this.stride = stride;
        this.bias = (float) Math.random() * 2 - 1;
    }

    public Filter(int padding, int stride, int[]... kernel) {
        super(kernel);
        this.padding = padding;
        this.stride = stride;
        this.bias = (float) Math.random() * 2 - 1;
    }

    public int getPadding() {
        return padding;
    }

    public int getStride() {
        return stride;
    }

    public double getBias() {
        return bias;
    }

    @Override
    public void randomize() {
        Random random = new Random();
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                matrix[i][j] = (random.nextInt(3) -1);
            }
        }
    }
}
