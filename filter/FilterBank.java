package filter;

import java.util.ArrayList;
import java.util.Random;

public class FilterBank{

    private static ArrayList<Filter> filters;

    public FilterBank(ArrayList<Filter> filters) {
        FilterBank.filters = filters;
    }

    public FilterBank() { }

    private static void init(){
        if(filters == null){
            filters = new ArrayList<>();
            filters.add(new Filter(0,1, new int[][]{{0,0,0},{0,0,0},{0,0,0}}));
            filters.add(new Filter(0,1, new int[][]{{1,1,1},{0,0,0},{-1,-1,-1}}));
            filters.add(new Filter(0,1, new int[][]{{-1,-1,-1},{-1,8,-1},{-1,-1,-1}}));

            //Generation de filtre test;
            Random random = new Random();
            for(int i = 0; i < 10; i++) {
                filters.add(new Filter(0, 1, new int[][]{{(random.nextInt(3) -1),(random.nextInt(3) -1),(random.nextInt(3) -1)
                },{(random.nextInt(3) -1),(random.nextInt(3) -1),(random.nextInt(3) -1)
                },{(random.nextInt(3) -1),(random.nextInt(3) -1),(random.nextInt(3) -1)
                } }));
            }
        }
    }

    public static Filter getFilter(int i){
        init();
        return filters.get(i);
    }

    public static int getSize(){
        init();
        return filters.size();
    }
}
